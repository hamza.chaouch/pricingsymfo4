<?php

namespace App\Tests\Service;

use App\Entity\Product;
use App\Entity\QueryParms;
use App\Service\PricingService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


/*
 *  • Si la stratégie est : x = 1 cent et y = 200 cents
 *  • Si les bornes de prix du vendeur pour cet article sont de 15 à 29,50€
 * */

class PricingServiceTest extends KernelTestCase
{


    /**
     * @var QueryParms
     */
    private $queryParm;

    private $pricingService;


    protected function setUp(): void
    {
        self::bootKernel();
        $this->pricingService = self::$container->get(PricingService::class);

        $this->queryParm = (new QueryParms())->setRef('AAAA')->setState(Product::NEW_STATE)
            ->setMinPrice(15)->setMaxPrice(29.50);
    }


    /**
     * tester si la methode marche avec toutes les data avec un meilleur etat on a la borne haute
     * S’il possède un exemplaire en « neuf », il devrait être à 29,50€ (le prix haut)
     */
    public function testmakePriceCalculWithCorrectData()
    {

        $res = $this->pricingService->makePriceCalcul($this->queryParm);

        $this->assertGreaterThan(0, $res);
        $this->assertEquals(29.5, $res);
    }

    /**
     * Pas de concurrence
     */
    public function testmakePriceCalculNoConcurrence()
    {

        // produit unique pas de concurrence
        $res = $this->pricingService->makePriceCalcul($this->queryParm->setRef('XXXX'));

        $this->assertGreaterThan(0, $res);
        $this->assertEquals($this->queryParm->getMaxPrice(), $res);

    }

    /**
     * avec  de la concurrence
     */
    public function testmakePriceCalculWithConcurrence()
    {

        // si le vendeur dispose d'un exemplaire en « Très bon état »,
        //  le programme devrait positionner l'article au prix de 19,99€.
        $res = $this->pricingService->makePriceCalcul($this->queryParm->setRef('AAAA')->setState(Product::VERY_WELL_STATE));

        $this->assertGreaterThan(0, $res);
        $this->assertEquals(19.99, $res);

        //S’il possède un exemplaire en « Bon état »,
        // il devrait être à 18€ (2 euros en dessous de Micro-jeux, qui a un état meilleur que le sien)
        $res = $this->pricingService->makePriceCalcul($this->queryParm->setState(Product::WELL_STATE));

        $this->assertGreaterThan(0, $res);
        $this->assertEquals(18, $res);


    }

    /**
     * S’il possède un exemplaire « comme neuf », il devrait être à 28,99€
     */
    public function testAsNew()
    {
        $res = $this->pricingService->makePriceCalcul($this->queryParm->setState(Product::AS_NEW_STATE));

        $this->assertGreaterThan(0, $res);
        $this->assertEquals(28.99, $res);
    }

}