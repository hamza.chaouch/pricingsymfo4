<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PricingControllerTest extends WebTestCase
{

    /**
     * Simuler une requète correct et s'attendre un comportement correcte
     */
    public function testCalculPricing(){
        $client = static::createClient();
        $client->request('POST','/api/pricing',[],[],['CONTENT_TYPE' => 'application/json'],
       '{
    "ref":"AAAA",
    "state":1,
    "max_price":29.50,
    "min_price":15
}'
        );

        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("29.5", $response->getContent());
        $this->assertStringContainsString("price", $response->getContent());

    }


    /**
     * passer un json avec une accolade manquante
     */
    public function testCalculPricingWhenJsonNotCorrect(){
        $client = static::createClient();
        $client->request('POST','/api/pricing',[],[],['CONTENT_TYPE' => 'application/json'],
            '{
    "ref":"AAAA",
    "state":1,
    "max_price":29.50,
    "min_price":15'
        );

        $response = $client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $this->assertStringContainsString("Syntax error", $response->getContent());

    }


    /**
     * passer un json avec une ref manquante
     */
    public function testCalculPricingWhenJsonNotCorrectMissingRef(){
        $client = static::createClient();
        $client->request('POST','/api/pricing',[],[],['CONTENT_TYPE' => 'application/json'],
            '{
    
    "state":1,
    "max_price":29.50,
    "min_price":15
    }'
        );

        $response = $client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $this->assertStringContainsString("violations", $response->getContent());

    }


}