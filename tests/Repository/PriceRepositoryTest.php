<?php

namespace App\Tests\Repository;

use App\Entity\QueryParms;
use App\Repository\PriceRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PriceRepositoryTest extends KernelTestCase
{


    /**
     * tester les cas de
     *   ->setState(3)->setMinPrice(15)->setMaxPrice(29.50)->setRef("AAAA"); resultat 3 concurrent
     * >setRef("BBBB")->setState(3)->setMinPrice(15)->setMaxPrice(29.50); pas de concurrence resultat 0
     * ->setRef("AAAA")->setState(3)->setMinPrice(20)->setMaxPrice(40); resultat 3 concurrent
     */
    public function testCountFindAllInInterval(){
        self::bootKernel();
        $queryParms = (new QueryParms())->setRef("AAAA")
            ->setState(3)
            ->setMinPrice(15)
            ->setMaxPrice(29.50);
        // product BBBB n'existe pas
        $queryParms1 = (new QueryParms())->setRef("BBBB")
            ->setState(3)
            ->setMinPrice(15)
            ->setMaxPrice(29.50);
        $queryParms2 = (new QueryParms())->setRef("AAAA")
            ->setState(3)
            ->setMinPrice(20)
            ->setMaxPrice(40);

        // de cette façon on peut meme getter les services privés
        $result = self::$container->get(PriceRepository::class)->findAllInInterval($queryParms);
        $result1 = self::$container->get(PriceRepository::class)->findAllInInterval($queryParms1);
        $result2 = self::$container->get(PriceRepository::class)->findAllInInterval($queryParms2);

        $this->assertEquals(3,count($result));
        $this->assertEquals(0,count($result1));
        $this->assertEquals(4,count($result2));

    }


}