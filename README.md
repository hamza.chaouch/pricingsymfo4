# _API Pricing Test_

## Modélisation:

J'ai choisi la modélisation suivante:

![Modelisationn de la base de données](./bdCapture.png "San Juan Mountains")

**Table Product:** 
1. id : clé primaire
2. ref : référence du produit chaine de caracteres
3. state : l'état du produit varie de 1->5 : 
   1. état neuf : constante 1
   2. état comme neuf : constante 2
   3. état très bon : 3
   4. état bon : 4
   5. état moyen : 5

**Table Seller:**
1. id clé primaire
2. name : nom du vendeur (concurrent)

**Table Price:** c'est une table associative avec l'info prix de vente
1. id clé primaire
2. sell_price : float prix de vente
3. seller_id : clé étrangère de la table seller
4. product_id : clé étrangère de la table product

## Documentation API: 
je vous ai crée un fichier swagger : 
dans la racine du projet : swagger.yaml

## Etapes pour mettre le projet en marche
1. git clone https://gitlab.com/hamza.chaouch/pricingsymfo4.git
2. composer install 
3. php bin/console d:d:c 
4. php bin/console make:migration
5. php bin/console d:m:m
6. php bin/console doctrine:fixtures:load (--env=test) 
7. Répétez les cmd precedentes avec le flag --env=test
8. lancer les tests unitaires : php bin/phpunit

## A-propos
J'ai utilisé : 
1. **Maker bundle**
2. **Symfony Profiler:** pour investiguer apropos les perf nom de requetes sql, utilisation mémoire etc...
3. **Documentation swagger** :
   [swgger editor lien](https://editor.swagger.io/)
   importer le fichier swagger.yaml dans la racine du projet
4. **la récurisivité** pour optimiser le calcul des prix
5. **validation**  validator bundle avec les expressions
6. **Gestion d'exception**
7. **Phpunit** pour les tests unitaires

## Mon code est il "**production-ready**" ?

**Non**, pas encore mon code n'est pas prèt pour faire la MEP il y'a des étapes dans le process qui manquent:
* **Code Review:**  Avant tout il faut faire de la code review afin d'avoir un deuxième avis que le mien.
* **Merger le job**
* **Lancer les tests unitaires du projet:**  ça nous permet de détecter des regressions si ça exite.
* **Mettre en PreProd**
* **Passer par une équipe de QA**
* **Tests de charge:** afin de voir les perf de ce bout de code. on peut utiliser blackfire pour avoir en retour un rapport détaillés
* **MEP:**  si tout va bien.