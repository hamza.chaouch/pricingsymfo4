<?php

namespace App\Service;

use App\Entity\QueryParms;
use App\Repository\PriceRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PricingService
{
    private $pricingRepository;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $prices = [];

    /**
     * @param PriceRepository $pricingRepository
     * @param ContainerInterface $container
     */
    public function __construct(PriceRepository $pricingRepository, ContainerInterface $container)
    {
        $this->pricingRepository = $pricingRepository;
        $this->container = $container;

    }

    /**
     * @param array $data
     * @param QueryParms $params
     * une fonction récrusive qui permet de mettre à jour le tableau
     * prices avec les prix selon chaque cas
     */
    public function priceFinder(array $data, QueryParms $params)
    {
        // si y'a pas  de concurrence return max price
        // empty price condition pour etre certain qu'on est dans la premiere itération
        if (empty($data) && empty($this->prices)) {
            $this->prices[] = (float)$params->getMaxPrice();
        }

        //s'il ya de la concurrence
        // la condition c'est pour eviter d'avoir un undifined offset après le array shift
        if (isset($data[0])) {

            //stratégie mis dans le fichier services.yml
            $x = $this->container->getParameter('app.stratégie_x');
            $y = $this->container->getParameter('app.stratégie_y');

            // création de ces varibales c'est juste pour la lisibilité
            // sinon on fera directement l'appel dans la condition sans avoir à créer des vars

            // état du produit concurrent à l'itération i
            $stateInDb = $data[0]['state'];
            // prix de vente du concurrent à l'itération i
            $priceInDb = $data[0]['sell_price'];

            // if egaulité de qualité on soustrait x
            if ($stateInDb === $params->getState()) {
                $this->prices[] = $priceInDb - $x;
            }

            //s' il ya un produit de qualité meilleure et avec un prix moins cher
            // s' il ya un produit de de qualité meilleur tout court
            if ($stateInDb < $params->getState() || $priceInDb < min($this->prices)) {
                $this->prices[] = $priceInDb - $y;
            }

            // passserr à litération suivante
            array_shift($data);

            //faire l'appel récursive
            $this->priceFinder($data, $params);
        }


    }

    /**
     * @param QueryParms $params
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function makePriceCalcul(QueryParms $params)
    {
        // recupérer les produits des concurrents suivant l'objet request
        $productsInDb = $this->pricingRepository->findAllInInterval($params);

        $this->priceFinder($productsInDb, $params);

        // le minimum du tableau car il contient tout les prix possibles (que ce soit
        // qualité egale ou meilleur etc ... ) du coup renvoyer toujours le meilleur prix
        return min($this->prices);


    }


// d'une façon non recursive mais  pas optimale
    /*  public function makePriceCalcul(QueryParms $params) : float
      {

          //stratégie mis dans le fichier services.yml
          $x= $this->container->getParameter('app.stratégie_x');
          $y= $this->container->getParameter('app.stratégie_y');

          $productsData =  $this->pricingRepository->findAllInInterval($params->getMinPrice(),$params->getMaxPrice(), $params->getRef(), $params->getState());

          foreach ($productsData as $product) {
              // return max
              //etat egale
             if($product['state'] === $params->getState()){
                 $resEqual [] = $product['sell_price']- $x;
                 //son etat est meilleur
             } elseif  ($product['state'] < $params->getState()){
                 $resBad [] = $product['sell_price']- $y;
             }

          }

         /* if (!empty($resEqual)){
              $min = min($resEqual);
              foreach ($productsData as $res){
                 if($res['sell_price'] <  $min && $res['state'] < $params->getState()){
                      $min=$res['sell_price']-$y < $params->getMinPrice()? $res['sell_price']:$res['sell_price']-$y;
                 }
              }
              return $min;
          }*/
    /*      if (!empty($resBad)){
              return min($resBad);
          }

          return $params->getMaxPrice();
  }*/

}