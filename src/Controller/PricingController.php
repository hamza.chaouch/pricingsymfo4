<?php

namespace App\Controller;

use App\Entity\QueryParms;
use App\Service\PricingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api")
 */
class PricingController extends AbstractController
{
    /**
     * @Route("/pricing", name="api_pricing", methods={"GET"})
     * injection de dépendances avec le service container de symfony pour avoir accés au repo product
     */
    /*public function index(PriceRepository $priceRepository): Response
    {
        // la method json va faire automatiquement les étape de normalization et puis la serialization
        // on a crée un context afin de retourner les valeurs du seller dans le produit
        // et eviter une boucle infinie (exple: prduit 1 a un seller 1 et seller 1 a un produit 1 ...)
        return $this->json($priceRepository->findAllInInterval(), 200, [], ["groups" => "read_group"]);
    }*/


    /**
     * @Route("/pricing", name="api_post_pricing", methods={"POST"})
     *
     */
    public function calculPricing(Request $request, SerializerInterface $serializer,ValidatorInterface $validator,PricingService $pricingService): \Symfony\Component\HttpFoundation\JsonResponse
    {
        try {
            // transformer le json reçu on un objet de type QueryParams
            // QueryParams c'est juste pour faciliter la manipulation de la request
            $params = $serializer->deserialize($request->getContent(), QueryParms::class, 'json');

            //system de validation avec des annotation dans l'entity QueryParm
            $errors = $validator->validate($params);
            if(count($errors)>0){
                return $this->json($errors, 400);

            }
            return $this->json(["price" => $pricingService->makePriceCalcul($params)], 200);

        } catch (\Exception $e  ) {

            return $this->json(["error" => $e->getMessage()], 400);

        }

    }
}
