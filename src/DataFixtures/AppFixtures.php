<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Seller;
use App\Entity\Price;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * fixture:load
 * pour charger la bd avec des données bidons
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        /*
         * je n'ai pas utilisé un bundle (faker) pour créer les données bidons rapidement
         * je les ai crées manuellement afin d'etre en syncro avec l'ennoncé de l'exercice */

        $seller1 =(new Seller())->setName('Abc jeux');
        $seller2 =(new Seller())->setName('Games-planet');
        $seller3 =(new Seller())->setName('micro-jeux');
        $seller4 =(new Seller())->setName('Top-jeux-video');
        $seller5 =(new Seller())->setName('Tous-les-jeux');
        $seller6 =(new Seller())->setName('Diffusion-123');
        $seller7 =(new Seller())->setName('France Video');

        $manager->persist($seller1);
        $manager->persist($seller2);
        $manager->persist($seller3);
        $manager->persist($seller4);
        $manager->persist($seller5);
        $manager->persist($seller6);
        $manager->persist($seller7);

        $product = (new Product())->setRef('AAAA')->setState(Product::MEDIUM_STATE);
        $product1 = (new Product())->setRef('AAAA')->setState(Product::MEDIUM_STATE);
        $product2 = (new Product())->setRef('AAAA')->setState(Product::VERY_WELL_STATE);
        $product3 = (new Product())->setRef('AAAA')->setState(Product::VERY_WELL_STATE);
        $product4 = (new Product())->setRef('AAAA')->setState(Product::WELL_STATE);
        $product5 = (new Product())->setRef('AAAA')->setState(Product::AS_NEW_STATE);
        $product6 = (new Product())->setRef('AAAA')->setState(Product::NEW_STATE);


        $manager->persist($product);
        $manager->persist($product1);
        $manager->persist($product2);
        $manager->persist($product3);
        $manager->persist($product4);
        $manager->persist($product5);
        $manager->persist($product6);

        $price = (new Price())->setSellPrice('14.10')->setSeller($seller1)->setProduct($product);
        $price1 = (new Price())->setSellPrice('16.20')->setSeller($seller2)->setProduct($product1);
        $price2 = (new Price())->setSellPrice('20')->setSeller($seller3)->setProduct($product2);
        $price3 = (new Price())->setSellPrice('21.50')->setSeller($seller4)->setProduct($product3);
        $price4 = (new Price())->setSellPrice('24.44')->setSeller($seller5)->setProduct($product4);
        $price5 = (new Price())->setSellPrice('29')->setSeller($seller6)->setProduct($product5);
        $price6 = (new Price())->setSellPrice('30.99')->setSeller($seller7)->setProduct($product6);

        $manager->persist($price);
        $manager->persist($price1);
        $manager->persist($price2);
        $manager->persist($price3);
        $manager->persist($price4);
        $manager->persist($price5);
        $manager->persist($price6);

        $manager->flush();
    }
}
