<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /*
     * annotation Groups c'est pour identifier les éléments a retourner dans le json
     * et pour qu'on ai pas le problem de boucle infinine lorsque symfony essai de serializer
     * les objets retournés
     */


    /**
     * Etat neuf dans la bd représenté par un entier 1
     */
    const NEW_STATE = 1;
    /**
     * Etat comme neuf dans la bd représenté par un entier 2
     */
    const AS_NEW_STATE = 2;
    /**
     * Etat très bon dans la bd représenté par un entier 3
     */
    const VERY_WELL_STATE = 3;
    /**
     * Etat Bon dans la bd représenté par un entier 4
     */
    const WELL_STATE = 4;
    /**
     * Etat Moyen dans la bd représenté par un entier 5
     */
    const MEDIUM_STATE = 5;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read_group")
     */
    private $id;



    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("read_group")
     */
    private $ref;

    /**
     * @ORM\Column(type="integer")
     * @Groups("read_group")

     *
     */
    private $state;



    /**
     * @ORM\OneToMany(targetEntity=Price::class, mappedBy="product")
     */
    private $prices;

    /**
     *
     */
    public function __construct()
    {
        $this->prices = new ArrayCollection();
    }



    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }







    /**
     * @return Collection|Price[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    /**
     * @param Price $price
     * @return $this
     */
    public function addPrice(Price $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
            $price->setProduct($this);
        }

        return $this;
    }

    /**
     * @param Price $price
     * @return $this
     */
    public function removePrice(Price $price): self
    {
        if ($this->prices->removeElement($price)) {
            // set the owning side to null (unless already changed)
            if ($price->getProduct() === $this) {
                $price->setProduct(null);
            }
        }

        return $this;
    }




}
