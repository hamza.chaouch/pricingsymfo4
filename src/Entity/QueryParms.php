<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * cette classe nous permet de mieux manipuler le content du lobjet request le desirealiser ddans cette objet
 */
class QueryParms
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min=4)
     */
    private  $ref;

    /**
     * @var int
     * @Assert\Range(
     *      min = 1,
     *      max = 5
     * )
     */
    private  $state;

    /**
     * @var float
     * @Assert\NotBlank
     * @Assert\PositiveOrZero
     * @Assert\Expression("value > this.getMinPrice()")
     */
    private  $maxPrice;

    /**
     * @var float
     * @Assert\NotBlank
     * @Assert\PositiveOrZero
     * @Assert\Expression("value < this.getMaxPrice()")
     */
    private  $minPrice;


    /**
     *
     */
    public function __construct()
    {
    }


    /**
     * @return string
     */
    public function getRef() : string
    {
        return $this->ref;
    }


    /**
     * @return string
     */
    public function getState() :string
    {
        return $this->state;
    }

    /**
     * @param mixed $ref
     */
    public function setRef($ref): self
    {
        $this->ref = $ref;
        return $this;

    }

    /**
     * @param mixed $state
     */
    public function setState($state): self
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @param mixed $maxPrice
     */
    public function setMaxPrice($maxPrice): self
    {
        $this->maxPrice = $maxPrice;
        return $this;

    }

    /**
     * @param mixed $minPrice
     */
    public function setMinPrice($minPrice): self
    {
        $this->minPrice = $minPrice;
        return $this;

    }


    /**
     * @return float
     */
    public function getMaxPrice() :float
    {
        return $this->maxPrice;
    }


    /**
     * @return float
     */
    public function getMinPrice():float
    {
        return $this->minPrice;
    }


}