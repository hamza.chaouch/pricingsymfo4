<?php

namespace App\Entity;

use App\Repository\PriceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PriceRepository::class)
 */
class Price
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read_group")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups("read_group")
     */
    private $sell_price;

    /**
     * @ORM\ManyToOne(targetEntity=Seller::class, inversedBy="prices")
     * @Groups("read_group")
     */
    private $seller;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="prices")
     * @Groups("read_group")
       */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSellPrice(): ?float
    {
        return $this->sell_price;
    }

    public function setSellPrice(float $sell_price): self
    {
        $this->sell_price = $sell_price;

        return $this;
    }

    public function getSeller(): ?Seller
    {
        return $this->seller;
    }

    public function setSeller(?Seller $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
