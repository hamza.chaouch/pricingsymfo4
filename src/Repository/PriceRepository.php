<?php

namespace App\Repository;

use App\Entity\Price;
use App\Entity\QueryParms;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Price|null find($id, $lockMode = null, $lockVersion = null)
 * @method Price|null findOneBy(array $criteria, array $orderBy = null)
 * @method Price[]    findAll()
 * @method Price[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Price::class);
    }



    /**
     * @param float $minPrice
     * @param float $maxPrice
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function findAllInInterval(QueryParms $params): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT seller.name as seller_name , seller.id as seller_id , 
                   product.id as product_id, product.ref, product.state, p.sell_price , p.id as price_id FROM price p
            INNER JOIN seller ON p.seller_id = seller.id
            INNER JOIN product ON p.product_id = product.id
            where (p.sell_price >= :min_price AND  p.sell_price <= :max_price 
            AND product.ref =:ref AND product.state <= :state )
            ';

        $stmt = $conn->prepare($sql);

        $stmt->execute(['min_price' => $params->getMinPrice() , 'max_price'=> $params->getMaxPrice(),
            'ref'=> $params->getRef(), 'state'=>$params->getState()]);

        // returns an array of arrays
        return  $stmt->fetchAllAssociative();
    }

    // /**
    //  * @return Price[] Returns an array of Price objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Price
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
